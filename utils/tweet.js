const entities = {"hashtags":[{"text":"request","indices":[18,26]},{"text":"BeforeYouGo","indices":[49,61]}],"urls":[{"url":"https://t.co/eXkQb0RxfB","expanded_url":"https://bit.ly/2Z0wrll","display_url":"bit.ly/2Z0wrll","indices":[84,107]}],"user_mentions":[{"screen_name":"LewisCapaldi","name":"Lewis Capaldi","id":370290486,"id_str":"370290486","indices":[35,48]}],"symbols":[],"media":[{"id":1220395350487380000,"id_str":"1220395350487379968","indices":[108,131],"media_url":"http://pbs.twimg.com/tweet_video_thumb/EO-3GvdWsAAi-xU.jpg","media_url_https":"https://pbs.twimg.com/tweet_video_thumb/EO-3GvdWsAAi-xU.jpg","url":"https://t.co/AxIMOCGZ2R","display_url":"pic.twitter.com/AxIMOCGZ2R","expanded_url":"https://twitter.com/New1027/status/1220395365591068674/photo/1","type":"photo","sizes":{"small":{"w":480,"h":324,"resize":"fit"},"thumb":{"w":150,"h":150,"resize":"crop"},"large":{"w":480,"h":324,"resize":"fit"},"medium":{"w":480,"h":324,"resize":"fit"}}}]}

const tweet = {
  created_at: 'Thu Jan 23 17:18:23 +0000 2020',
  id: 1220395365591068700,
  id_str: '1220395365591068674',
  text: 'Julie dm’d with a #request for New @LewisCapaldi #BeforeYouGo!  You got it!  On now https://t.co/eXkQb0RxfB https://t.co/AxIMOCGZ2R 😃😎',
  display_text_range: [ 0, 107 ],
  source: '<a href="http://twitter.com/download/iphone" rel="nofollow">Twitter for iPhone</a>',
  truncated: false,
  in_reply_to_status_id: null,
  in_reply_to_status_id_str: null,
  in_reply_to_user_id: null,
  in_reply_to_user_id_str: null,
  in_reply_to_screen_name: null,
  user: {
    id: 21667364,
    id_str: '21667364',
    name: 'NEW🗽102.7',
    screen_name: 'New1027',
    location: 'NYC',
    url: 'http://www.NEW1027.com',
    description: 'ON-AIR: @KarenWNEW 6-10a | @EmilyWestTV 10a-3p | @MikeAdamOnAir 3-7p | @XtineRichie 7p-12a | Stream us w/ the @RadioDotCom app: http://bit.ly/2O6oNRq',
    translator_type: 'none',
    protected: false,
    verified: true,
    followers_count: 14340,
    friends_count: 726,
    listed_count: 381,
    favourites_count: 17107,
    statuses_count: 50830,
    created_at: 'Mon Feb 23 16:44:36 +0000 2009',
    utc_offset: null,
    time_zone: null,
    geo_enabled: true,
    lang: null,
    contributors_enabled: false,
    is_translator: false,
    profile_background_color: '30797A',
    profile_background_image_url: 'http://abs.twimg.com/images/themes/theme1/bg.png',
    profile_background_image_url_https: 'https://abs.twimg.com/images/themes/theme1/bg.png',
    profile_background_tile: false,
    profile_link_color: '30797A',
    profile_sidebar_border_color: 'FFFFFF',
    profile_sidebar_fill_color: '125450',
    profile_text_color: '668F8D',
    profile_use_background_image: false,
    profile_image_url: 'http://pbs.twimg.com/profile_images/1018877549990932485/dQDTNxq0_normal.jpg',
    profile_image_url_https: 'https://pbs.twimg.com/profile_images/1018877549990932485/dQDTNxq0_normal.jpg',
    profile_banner_url: 'https://pbs.twimg.com/profile_banners/21667364/1552080130',
    default_profile: false,
    default_profile_image: false,
    following: null,
    follow_request_sent: null,
    notifications: null
  },
  geo: null,
  coordinates: null,
  place: null,
  contributors: null,
  is_quote_status: false,
  quote_count: 0,
  reply_count: 0,
  retweet_count: 0,
  favorite_count: 0,
  entities,
  extended_entities: { media: [ [Object] ] },
  favorited: false,
  retweeted: false,
  possibly_sensitive: false,
  filter_level: 'low',
  lang: 'en',
  timestamp_ms: '1579799903663'
}

module.exports = { tweet, entities }
