// Output the data here
const chalk = require('chalk')

// This is basically one huge string literal, so indentation has to be weird
const output = (stats) => {
  console.clear()
  console.log(chalk`
{bold.blue Stats since ${stats.started}}

{bold Total tweets:}\t{green ${stats.total}}

{bold AVERAGES}
{yellow hour:}\t{green ${stats.averages.hour}}
{yellow minute:}\t{green ${stats.averages.minute}}
{yellow second:}\t{green ${stats.averages.second}}

{white PERCENT CONTAINING:}
{yellow emoji:}\t{green ${stats.percentages.emoji}%}
{yellow urls:}\t{green ${stats.percentages.url}%}
{yellow photos:}\t{green ${stats.percentages.photo}%}

{bold MOST POPULAR}
{yellow emoji:}
{green   ${stats.tops.emoji.join('\n  ')}}

{yellow hashtags:}
{green   ${stats.tops.hashtags.join('\n  ')}}

{yellow domains:}
{green   ${stats.tops.domains.join('\n  ')}}
`)
}

module.exports = output
