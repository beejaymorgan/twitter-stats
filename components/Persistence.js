// We're going to fake persistence right now, and just store the object in memory.
// If you're using a document store like Mongo, this would be simple to flesh out.
// We have a flux-ish data flow here. 
// Though, really, what would be best is to dump all the tweets straight into Elastic Search,
// or Big Query, or... and then do your aggregations from there.

// • Total number of tweets received
// • Average tweets per hour/minute/second
// • Top emojis in tweets*
// • Percent of tweets that contains emojis
// • Top hashtags
// • Percent of tweets that contain a url
// • Percent of tweets that contain a photo url (pic.twitter.com or
//   instagram)
// • Top domains of urls in tweets

// Analytics are complicated. We'll do some basic things with the "top" lists:
// - remove anything that hasn't been seen in LIFETIME milliseconds
// - sort by the count (duh)
// - trim it down to the last 10,000 items captured, just to keep from leaking  memory 


// We'll make this a constant, since we can still mutate its props
const stats = {
  totalTweets: 0,
  totals: {
    emoji: 0,
    url: 0,
    photo: 0
  }
}

let hashtags = []
let domains = []
let emoji = []

const updateTotal = (toAdd) => {
  // Add some validation. We'll keep it simple.
  if (isNaN(toAdd) || toAdd < 1) {
    throw new Error(`Invalid value ${toAdd} passed as param. Must be a positive integer`)
  }
  stats.totalTweets = stats.totalTweets + toAdd
}

const addHashtags = (tags = []) => {
  const timestamp = Date.now()
  tags.forEach(item => {
    hashtags.push({
      item,
      timestamp
    })
  })
}

const addDomains = (hosts = []) => {
  const timestamp = Date.now()
  hosts.forEach(item => {
    domains.push({
      item,
      timestamp
    })
  })
  if (hosts.length) {
    stats.totals.url = stats.totals.url + 1
  }
}

const addEmoji = (symbols = []) => {
  const timestamp = Date.now()
  symbols.forEach(item => {
    emoji.push({
      item,
      timestamp
    })
  })
  if (symbols.length) {
    stats.totals.emoji = stats.totals.emoji + 1
  }
}

// So, if one of the URLs looked like a photo URL, count it as a photo
// This would definitely get refactored at some point
const addPhoto = (hasPhoto) => {
  if (!hasPhoto) { return }
  stats.totals.photo = stats.totals.photo + 1
}

// Return a copy of stats so it can't be mutated somewhere else
// A little messy, but after all, we're keeping this in memory.
const getStats = () => {
  return {
    total: stats.totalTweets,
    averages: {...stats.averages},
    totals: {...stats.totals}
  }
}

const getHashtags = () => {
  return hashtags
}

const getDomains = () => {
  return domains
}

const getEmoji = () => {
  return emoji
}

const pruneTops = (ts) => {
  hashtags = hashtags.filter(h => h.timestamp > ts)
  emoji = emoji.filter(e => e.timestamp > ts)
  domains = domains.filter(d => d.timestamp > ts)
}

module.exports = {
  addEmoji,
  getEmoji,
  addHashtags,
  getHashtags,
  addDomains,
  getDomains,
  addPhoto,
  updateTotal,
  getStats,
  pruneTops
}
