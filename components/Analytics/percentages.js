// This component calculates the percentages of tweets containing different metadata

const calcPer = (part, whole) => {
  return Math.floor((part / whole) * 100)
}

const getPercentages = (data) => {
  const { total, totals } = data
  return {
    emoji: calcPer(totals.emoji, total),
    url: calcPer(totals.url, total),
    photo: calcPer(totals.photo, total)
  }
}

module.exports = getPercentages
