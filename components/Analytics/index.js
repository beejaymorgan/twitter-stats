// This will be our analytics component.
// We're requiring the persistence component here, though that's dangerous
// In a real production app, this component will pull data directly from the
// analytics DB

// We'll keep this here, just because we can.
const START = new Date()

// How long we want to keep things in the tops
// Anything in the most popular list is only the most popular for this lifespan
const LIFESPAN = 1000 * 60 * 10 // 10 minutes

const persistence = require('../Persistence')
const percentages = require('./percentages')
const averages = require('./averages')
const tops = require('./tops')

const getData = async () => {
  const data = persistence.getStats()
  const emoji = persistence.getEmoji()
  const hashtags = persistence.getHashtags()
  const domains = persistence.getDomains()
  persistence.pruneTops(Date.now() - LIFESPAN)
  return {
    ...data,
    started: START.toLocaleString(),
    averages: averages(data.total, Date.now(), START),
    percentages: percentages(data),
    tops: {
      emoji: tops(emoji, 10),
      hashtags: tops(hashtags, 10),
      domains: tops(domains, 10)
    }
  }
}

module.exports = {
  getData
}
