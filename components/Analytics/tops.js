// Takes a list of lots of data and trims it down to the top ones

const getTopItems = (list, size) => {
  // First, count the unique entries
  let counts = {}
  list.map(l => l.item).forEach(l => {
    counts[l] = (counts[l] || 0) + 1
  })
  // Turn that bad boy into an array!
  counts = Object.entries(counts)
  // Now sort it, slice it, and wrap it with a bow
  return counts
    .sort((a, b) => b[1] - a[1])
    .slice(0, size)
    .map(([k, v]) => `${k} (${v})`)
}

module.exports = getTopItems
