// This component calculates the time-based averages for tweets

// Just some convenience numbers
const AVG = {
  hr: 1000 * 60 * 60,
  mn: 1000 * 60,
  sc: 1000
}

const getAverages = (total, ts, start) => {
  const timeDiff = ts - start.getTime()
  const seconds = timeDiff / AVG.sc
  const minutes = timeDiff / AVG.mn
  const hours = timeDiff / AVG.hr
  return {
    hour: hours >= 1 ? Math.floor(total / hours) : 'N/A',
    minute: minutes >= 1 ? Math.floor(total / minutes) : 'N/A',
    second: Math.floor(total / seconds)
  }
}

module.exports = getAverages
