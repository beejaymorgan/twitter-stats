// Simple function to send back what we want

const cleanHashtags = (hashtags) => {
  return hashtags.map(ht => ht.text)
}

module.exports = cleanHashtags
