// Since we get whole URLs, we want to clean those up to just domain names
// and remove dupes in a single tweet
// Also, ignore twitter.com, because that's boring
const cleanUrls = (urls) => {
  const hosts = urls.map(url => {
    const parsedUrl = new URL(url.expanded_url)
    return parsedUrl.host
  }) // .filter(host => host !== 'twitter.com')
  // Using Set ensures they're unique
  return [...new Set(hosts)]
}

module.exports = cleanUrls
