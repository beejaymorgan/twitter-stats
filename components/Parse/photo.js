// Check those URLs to see if they match any photo stuff
// (if you solve a problem with regex, you now have two problems...)
// We'll check both the display URL and the expanded URL to see if it's some sort of image
// We'll also check the media array to see if it contains a photo

const checkPhotos = (urls, media = []) => {
  const joined = urls.map(u => `${u.expanded_url}|${u.display_url}`).join('|')
  const hasPhotoUrl = /pic.twitter|instagram|gif|jpg|jpeg|png/g.test(joined)
  const hasPhotoMedia = media.some(m => m.type === 'photo')
  return hasPhotoMedia || hasPhotoUrl
}

module.exports = checkPhotos
