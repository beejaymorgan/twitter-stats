// Parse the tweet and see what's in it
const findEmoji = require('./emoji')
const cleanUrls = require('./urls')
const cleanHashtags = require('./hashtags')
const checkPhoto = require('./photo')

const parse = async (tweet) => {
  if (!tweet || !tweet.entities) {
    return {
      hasPhoto: false,
      emoji: [],
      urls: [],
      hashtags: []
    }
  }
  const { hashtags, urls, media } = tweet.entities
  return {
    hasPhoto: checkPhoto(urls, media),
    emoji: findEmoji(tweet.text),
    urls: cleanUrls(urls),
    hashtags: cleanHashtags(hashtags)
  }
}

module.exports = parse
