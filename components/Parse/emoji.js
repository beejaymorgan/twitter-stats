// This is kind of old and probably doesn't find them all
// It also introduces a vulnerability, so shouldn't be used in production code
const EmojiData = require('emoji-data')

const findEmoji = (text) => {
  const emoji = EmojiData.scan(text)
  return [...new Set(emoji.map(e => e.short_name))]
}

module.exports = findEmoji
