// We'll use a lib here, though it would be way more fun to build our own.
// The fewer dependencies the better, amirite?
const Twitter = require('twitter')

// Initialize the tweeterz
const twitter = new Twitter({
  consumer_key: process.env.API_KEY,
  consumer_secret: process.env.API_SECRET_KEY,
  access_token_key: process.env.ACCESS_TOKEN,
  access_token_secret: process.env.ACCESS_TOKEN_SECRET
})

// Export a function that can set up the listener and pass back the results
// We're passing callbacks here. Other approaches:
// - Return the stream itself, and attach listeners in the calling component
// - Make a class. But we're not doing that, because OOP is... messy. :-)
const createStream = (onData, onError) => {
  twitter.stream('statuses/sample', stream => {
    stream.on('data', onData)
    stream.on('error', onError)
  })
}

module.exports = createStream
