# Twitter Analytics Test

This is a Node app that listens to the Twitter stream sample and does a little bit of fiddly parsing and analytics on it.

It was built in Node 12.x, but will likely run in anything 8.x or greater.

NPM packages used:

- **Chalk** -- This package makes coloring text in the console simple.
- **dotenv** -- Allows grabbing settings from an `.env` file in the root
- **emoji-data** -- Parses text and looks for emoji characters. This package is pretty old and should probably be replaced.
- **twitter** -- Connects to the twitter API. Honestly, this is probably unnecessary, as it would be just as easy to just poll the endpoint once a second or so.

_dev dependencies_

- **nodemon** -- A super simple watcher that restarts the app when changes are made
- **jest** -- testing framework

## Running the app

When running, the app continually updates the console with real-time stats.

- `cp .env.sample .env`
- Fill in the `.env` file with your Twitter API credentials
- `npm install`
- To run in production mode, `npm start`
- To run in dev mode, `npm run dev`
- To run the tests, `npm run test` (or `jest --watch` when developing)

### Observations

I used arrow functions with constants rather than function declarations in this app. The jury seems to still be out on whether one really outweighs the other, so I did the whole thing this way to see if I would run into any problems. I didn't. Neither did I find it more compelling. Regardless, it's best to match the style of the code that's already there anyways.

There are bits and pieces of the code that run async. It doesn't really make much difference, because there's no load on the app with just the sample stream. I would be interested to see what would happen if it were ingesting the firehose.

### Microservices

This app is really just a Proof of Concept. In a production setting, the ingestor and aggregators would be separate microservices, and likely connected through an Elastic Search or Big Query data store. The popular content in this app is only stored for 10 minutes, and then deleted from disk. This seems to end up hovering around 60MB of memory on my system. YMMV.