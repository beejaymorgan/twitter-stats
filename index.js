/* 
Entry point for the twitter stats Script
Tracks the following:
• Total number of tweets received
• Average tweets per hour/minute/second
• Top emojis in tweets*
• Percent of tweets that contains emojis
• Top hashtags
• Percent of tweets that contain a url
• Percent of tweets that contain a photo url (pic.twitter.com or
instagram)
• Top domains of urls in tweets
*/

// Change the process name so we can find it!
process.title = "Node Twitter Stats"

// Load the .env file
require('dotenv').config()

// Load dependencies
const createStream = require('./components/TwitterStream')
const output = require('./components/Output')
const persistence = require('./components/Persistence')
const parse = require('./components/Parse')
const analytics = require('./components/Analytics')

// Keep a run state here so we can cancel the output interval if we need to
let running = true

// Set up the twitter stream listener
createStream(data => {
  parse(data)
    .then(({ hashtags, urls, emoji, hasPhoto }) => {
      persistence.updateTotal(1)
      persistence.addHashtags(hashtags)
      persistence.addDomains(urls)
      persistence.addEmoji(emoji)
      persistence.addPhoto(hasPhoto)
    })
    .catch(error => {
      running = false
      console.error('Error parsing tweet data')
      console.error(error)
    })
}, error => {
  running = false
  console.error(error)
})

const updateUI = () => {
  // If we want to kill it, kill it before it clears the console again
  if (!running) { return }
  // Analytics are run async so as not to block incoming data
  // In a real app, analytics would be an entire microservice on its own :-)
  analytics.getData()
    .then(data => {
      output(data)
      setTimeout(updateUI, 1000)
    })
}

updateUI()
