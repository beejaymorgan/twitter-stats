const parse = require('../components/Parse')
const findEmoji = require('../components/Parse/emoji')
const cleanUrls = require('../components/Parse/urls')
const cleanHashtags = require('../components/Parse/hashtags')
const checkForPhoto = require('../components/Parse/photo')
const { tweet, entities } = require('../utils/tweet')

test('Clean hashtags', () => {
  const hashtags = cleanHashtags(entities.hashtags)
  expect(hashtags.length).toBe(2)
  expect(hashtags[0]).toBe('request')
})

test('Clean URLs', () => {
  const domains = cleanUrls(entities.urls)
  expect(domains.length).toBe(1)
  expect(domains[0]).toBe('bit.ly')
})

test('Find emoji', () => {
  const emoji = findEmoji(tweet.text)
  expect(emoji.length).toBe(2)
  expect(emoji[1]).toBe('sunglasses')
})

test('Check for photo', () => {
  const hasPhoto = checkForPhoto(entities.urls, entities.media)
  expect(hasPhoto).toBe(true)
})

test('Parse tweet', async () => {
  const parsed = await parse(tweet)
  expect(parsed).toBeTruthy()
  expect(parsed).toHaveProperty('emoji', [
    'smiley',
    'sunglasses'
  ])
  expect(parsed).toHaveProperty('urls', ['bit.ly'])
  expect(parsed).toHaveProperty('hashtags', [
    'request',
    'BeforeYouGo'
  ])
})
