const analytics = require('../components/Analytics')
const persistence = require('../components/Persistence')
const { tweet, entities } = require('../utils/tweet')

test('Analytics object structure', async () => {
  const data = await analytics.getData()
  expect(data).toMatchObject({
    started: expect.any(String),
    total: 0,
    averages: {
      hour: 'N/A',
      minute: 'N/A',
      second: 0
    },
    totals: {
      emoji: 0,
      url: 0,
      photo: 0
    },
    tops: {
      emoji: expect.any(Array),
      hashtags: expect.any(Array),
      domains: expect.any(Array)
    },
    percentages: {
      emoji: expect.any(Number),
      url: expect.any(Number),
      photo: expect.any(Number)
    }
  })
})

test('Get percentages', async () => {
  persistence.updateTotal(1)
  persistence.addHashtags(['tag'])
  persistence.addDomains(['google.com'])
  persistence.addEmoji(['joy'])
  persistence.addPhoto(true)
  const data = await analytics.getData()
  expect(data.totals).toMatchObject({
    emoji: 1,
    url: 1,
    photo: 1
  })
  expect(data.percentages).toMatchObject({
    emoji: 100,
    url: 100,
    photo: 100
  })
})

test('Get top metadata', async () => {
  const data = await analytics.getData()
  expect(data.tops).toMatchObject({
    emoji: expect.arrayContaining(['joy (1)']),
    domains: expect.arrayContaining(['google.com (1)']),
    hashtags: expect.arrayContaining(['tag (1)'])
  })
})

test('Prune metadata lists', async () => {
  persistence.pruneTops(Date.now())
  const data = await analytics.getData()
  expect(data.tops).toMatchObject({
    emoji: [],
    domains: [],
    hashtags: []
  })
})
