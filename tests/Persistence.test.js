const persistence = require('../components/Persistence')

test('Increment the tweet count', () => {
  expect(persistence.getStats()).toHaveProperty('total', 0)
  persistence.updateTotal(1)
  expect(persistence.getStats()).toHaveProperty('total', 1)
})

test('Add an emoji', () => {
  expect(persistence.getStats()).toHaveProperty('totals.emoji', 0)
  expect(persistence.getEmoji()).toHaveLength(0)
  const emoji = 'joy'
  persistence.addEmoji([emoji])
  expect(persistence.getStats()).toHaveProperty('totals.emoji', 1)
  expect(persistence.getEmoji()).toMatchObject([
    {item: emoji}
  ])
})

test('Add a hashtag', () => {
  expect(persistence.getHashtags()).toHaveLength(0)
  const tag = 'testMuhStuff'
  persistence.addHashtags([tag])
  expect(persistence.getHashtags()).toMatchObject([
    {item: tag}
  ])
})

test('Add a domain', () => {
  expect(persistence.getStats()).toHaveProperty('totals.url', 0)
  expect(persistence.getDomains()).toHaveLength(0)
  const domain = 'google.com'
  persistence.addDomains([domain])
  expect(persistence.getStats()).toHaveProperty('totals.url', 1)
  expect(persistence.getDomains()).toMatchObject([
    {item: domain}
  ])
})

test('Add a photo', () => {
  expect(persistence.getStats()).toHaveProperty('totals.photo', 0)
  persistence.addPhoto(true)
  expect(persistence.getStats()).toHaveProperty('totals.photo', 1)
})
